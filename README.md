# TP: crypto

## Binome

Collard, Esteban, email: esteban.collard@univ-lille.fr   
Finard, Alexis, email: alexis.finard.etu@univ-lille.fr

## Question 1

Notre solution consiste à utiliser l'opérateur "xor" entre le responsable un et deux afin d'obtenir une clé qui servira à crypter et à décrypter le contenu de `DISK/`. Plus précisément, chaque responsable possédera une clé usb (ici `USB1/` et `USB2/`) qui contiendra une clé générée de manière aléatoire. En plus de cela, chaque responsable aura un mot de passe. Le mot de passe sera haché (à l'aide de shasum -a 256) puis l'opérateur "xor" sera utilisé entre le mot de passe haché et le contenu de USB. Ce résultat est la clé du responsable, l'opérateur "xor" sera ensuite utilisé entre les clés des deux responsables afin de produire la clé finale.

## Question 2

`./init.py` permet d'initialiser l'environnement ainsi que les mots de passe   
`./service.py` permet de lancer un service si le décryptage est réussi

## Question 3
Les représentants auront dans leurs clés usb le contenu de la clé usb du responsable, ainsi que le mot de passe du responsable qui sera, quant à lui, crypté à l'aide du mot de passe du représentant. 
Lors de l'appel à un service, il faudra spécifier si on est responsable ou représentant.

## Question 5
Lors de l'appel à la répudiation, le contenu de DISK sera décrypté puis recrypté à l'aide d'une nouvelle clé, les informations contenues dans les `USB/` seront réactualisés