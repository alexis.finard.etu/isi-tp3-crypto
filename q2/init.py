#! /usr/bin/env python3.5
import os

# Creation des dossiers
def create_env():
    if not os.path.exists('env'):
        os.makedirs('env')
        
    if not os.path.exists("env/USB1"):
        os.makedirs("env/USB1")

    if not os.path.exists("env/USB2"):
        os.makedirs("env/USB2")

    if not os.path.exists("env/RAMDISK"):
        os.makedirs("env/RAMDISK")

    if not os.path.exists('env/DISK'):
        os.makedirs("env/DISK")

    if not os.path.exists('env/DISK/data'):
        os.system('touch env/RAMDISK/data')
        empty_data=open("env/RAMDISK/data","w")
        empty_data.write("INIT\n")
        empty_data.close()
        
    if not os.path.exists("env/USB1/key"):
        os.system('touch env/USB1/key')
        
    if not os.path.exists("env/USB2/key"):
        os.system('touch env/USB2/key')






# Demande des mots de passe
def create_password():
    mdp1= input("Veulliez saisir le mot de passe 1: ")
    mdp2= input("Veulliez saisir le mot de passe 2: ")

    shasum1 = "echo -n'" + mdp1 + "' | shasum -a 256 > env/RAMDISK/mdp1"
    shasum2 = "echo -n'" + mdp2 + "' | shasum -a 256 > env/RAMDISK/mdp2"

    os.system(shasum1)
    os.system(shasum2)
    


# Fais un Xor entre toutes les clés afin d'obtenir la clé finale
def create_final_key():
    usb1 = open("env/USB1/key")
    usb2 = open("env/USB2/key")
    mdp1 = open("env/RAMDISK/mdp1")
    mdp2 = open("env/RAMDISK/mdp2")
    finalkey= open("env/RAMDISK/finalkey","w")

    value1 = usb1.read(1)

    while value1 != "":
        value1 = int(value1,16)
        value2 = int(mdp1.read(1),16)
        first_xor = value1 ^ value2
        value3 = int(usb2.read(1),16)
        second_xor = first_xor ^ value3
        value4 = int(mdp2.read(1),16)
        last_xor = second_xor ^ value4
        finalkey.write(hex(last_xor).rstrip("L").lstrip("0x") or "0")
        value1= usb1.read(1)

    usb1.close()
    usb2.close()
    mdp1.close()
    mdp2.close()
    finalkey.close()

def main():

    create_env()

    # creation des clés dans USB

    os.system("dd if=/dev/urandom bs=32 count=1 | hexdump -ve '1/1 \"%02x\"'> env/USB1/key")
    os.system("dd if=/dev/urandom bs=32 count=1 | hexdump -ve '1/1 \"%02x\"'> env/USB2/key")

    create_password()

    create_final_key()

    os.system('openssl enc -e -aes-256-ecb -in env/RAMDISK/data -out env/DISK/data -K $(cat env/RAMDISK/finalkey)')

    
    os.system('rm -f env/RAMDISK/*')
    




if __name__ == "__main__":
    # execute only if run as a script
    main()
