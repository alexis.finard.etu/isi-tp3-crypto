#! /usr/bin/env python3.5
import os
import init
def addPair(name, number):
    fichier = open("env/RAMDISK/data", "a")
    fichier.write(name+":"+number+"\n")
    fichier.close()

def removePair(name, number):
    fichier = open("env/RAMDISK/data","r")
    lines = fichier.readlines()
    fichier.close()
    fichier = open("env/RAMDISK/data","w")
    for line in lines:
        if line!=name+":"+number+"\n":
            fichier.write(line)
    fichier.close()

def findPair(name):
    fichier = open("env/RAMDISK/data","r")
    lines = fichier.readlines()
    fichier.close()
    for line in lines:
        if(line.startswith(name+":")):
            print(line)
        
def cryptData():
    os.system('openssl enc -e -aes-256-ecb -in env/RAMDISK/data -out env/DISK/data -K $(cat env/RAMDISK/finalkey)')
    os.system('rm -f env/RAMDISK/*')
    
def decryptData():
    return (os.system('openssl enc -d -aes-256-ecb -in env/DISK/data -out env/RAMDISK/data -K $(cat env/RAMDISK/finalkey)'))



def main():
    if not os.path.exists('env/USB1/key'):
        print("La clé 1 n'est pas présente")
        return
    if not os.path.exists('env/USB2/key'):
        print("La clé 2 n'est pas présente")
        return

    init.create_password()
    init.create_final_key()

    if decryptData() != 0:
        os.system('rm -f env/RAMDISK/*')
        print("ECHEC DU DECRYPTAGE")
        return;

    answer = "False"

    while (not(answer.isdigit()) or int(answer)>3):
        answer = input("IDENTIFICATION REUSSIE, QUE VOULEZ VOUS ?\n\t Ajouter une paire : 1\n\t Enlever une paire : 2\n\t Trouver une paire : 3\n")


    answer = int(answer)

    if (answer == 1):
        name = input("Donnez nous le nom à ajouter: ")
        number = "False"
        while(not(number.isdigit())):
            number = input("Donnez nous le numéro à ajouter: ")
        addPair(name, number)
    elif (answer == 2):
        name = input("Donnez nous le nom à enlever: ")
        number = "False"
        while(not(number.isdigit())):
            number = input("Donnez nous le numéro à enlever: ")        
        removePair(name, number)
    else:
        name = input("Donnez nous le nom à chercher: ")
        findPair(name)
    cryptData()
if __name__ == "__main__":
    # execute only if run as a script
    main()
