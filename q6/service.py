#! /usr/bin/env python3.5
import os
import init
def addPair(name, number):
    fichier = open("env/RAMDISK/data", "a")
    fichier.write(name+":"+number+"\n")
    fichier.close()

def removePair(name, number):
    fichier = open("env/RAMDISK/data","r")
    lines = fichier.readlines()
    fichier.close()
    fichier = open("env/RAMDISK/data","w")
    for line in lines:
        if line!=name+":"+number+"\n":
            fichier.write(line)
    fichier.close()

def findPair(name):
    fichier = open("env/RAMDISK/data","r")
    lines = fichier.readlines()
    fichier.close()
    for line in lines:
        if(line.startswith(name+":")):
            print(line)
        
def cryptData():
    os.system('openssl enc -e -aes-256-ecb -in env/RAMDISK/data -out env/DISK/data -K $(cat env/RAMDISK/finalkey)')
    os.system('rm -f env/RAMDISK/*')
    
def decryptData():
    return (os.system('openssl enc -d -aes-256-ecb -in env/DISK/data -out env/RAMDISK/data -K $(cat env/RAMDISK/finalkey)'))


def nulldecrypt(path,isrepr):
    password = input("Mot de passe: ")
    shasum = "echo -n'" + password + "' | shasum -a 256 > env/RAMDISK/shasum"
    os.system(shasum)
    if isrepr: # si c'est un représentant        
        return False if (os.system('openssl enc -d -aes-256-ecb -in ' + path+'pass -out /dev/null -K $(cat env/RAMDISK/shasum)')) != 0 else True
    else:
        # verifier si le shasum est égal à mdp1/2
        pass
    


def repudiation(): 
    os.system('openssl enc -d -aes-256-ecb -in env/DISK/number -out env/RAMDISK/number -K $(cat env/RAMDISK/finalkey)')
    num = open("env/RAMDISK/number")
    number = int(num.readline()) - 3 # -2 pour ceux déjà connecté et -1 pour celui qui sera répudié
    finalnumber = number + 2
    num.close()

    print("il manque encore " + str(number) + " personnes pour la répudiation")

    connected = (False, False) # True si le responsable est connecté
    if not os.path.exists('env/RAMDISK/reprmdp1'):
        connected = (True,False)
    if not os.path.exists('env/RAMDISK/reprmdp2'):
        connected = (connected[0],True)

    i = 0
    while (i != number):
        answer = "False"
        while (not(answer.isdigit()) or int(answer)>4):
            answer = input("Identifiez vous : \n\t responsable 1 : 1\n\t représentant 1: 2\n\t responsable 2: 3\n\t représentant 2: 4\n:")
        answer = int(answer)

        
        if answer == 1:
            if (not os.path.exists('env/USB1/key')):
                print("La clé du responsable 1 n'est pas présente")
                return
            if connected[0]:
                print("L'individu est déjà présent")
            else:
                if not nulldecrypt('env/USB1/',False):
                    print("Erreur de clé")
                    return
                connected = (True,connected[1])
                i+=1
        elif answer ==2:
            if (not os.path.exists('env/REPRUSB1/key')):
                print("La clé du représentant 1 n'est pas présente")
                return
            if (os.path.exists('env/RAMDISK/reprmdp1')):
                print("L'individu est déjà présent")
            else:
                if not nulldecrypt('env/REPRUSB1/',True):
                    print("Erreur de clé")
                    return
                i+=1
        elif answer ==3:
            if (not os.path.exists('env/USB2/key')):
                print("La clé du responsable 2 n'est pas présente")
                return
            if connected[1]:
                print("L'individu est déjà présent")
            else:
                if not nulldecrypt('env/USB2/',False):
                    print("Erreur de clé")
                    return
                i+=1
                connected = (connected[0],True)
        else:
            if (not os.path.exists('env/REPRUSB2/key')):
                print("La clé du représentant 2 n'est pas présente")
                return
            if (os.path.exists('env/RAMDISK/reprmdp2')):
                print("L'individu est déjà présent")
            else:
                if not nulldecrypt('env/REPRUSB2/',True):
                    print("Erreur de clé")
                    return
                i+=1


    num = open("env/RAMDISK/number","w")
    num.write(str(finalnumber))
    num.close()
    os.system('openssl enc -e -aes-256-ecb -in env/RAMDISK/number -out env/DISK/number -K $(cat env/RAMDISK/finalkey)')

def main():
    if (not os.path.exists('env/USB1/key')) and (not os.path.exists('env/REPRUSB1/key')) :
        print("La clé 1 n'est pas présente")
        return
    if (not os.path.exists('env/USB2/key')) and (not (os.path.exists('env/REPRUSB2/key'))):
        print("La clé 2 n'est pas présente")
        return

    if not init.create_password():
        os.system('rm -f env/RAMDISK/*')
        print("ECHEC DU DECRYPTAGE")
        return

    init.create_final_key_with_repr()

    if decryptData() != 0:
        os.system('rm -f env/RAMDISK/*')
        print("ECHEC DU DECRYPTAGE")
        return;

    answer = "False"

    while (not(answer.isdigit()) or int(answer)>4):
        answer = input("IDENTIFICATION REUSSIE, QUE VOULEZ VOUS ?\n\t Ajouter une paire : 1\n\t Enlever une paire : 2\n\t Trouver une paire : 3\n\t Répudiation : 4")


    answer = int(answer)

    if (answer == 1):
        name = input("Donnez nous le nom à ajouter: ")
        number = "False"
        while(not(number.isdigit())):
            number = input("Donnez nous le numéro à ajouter: ")
        addPair(name, number)
    elif (answer == 2):
        name = input("Donnez nous le nom à enlever: ")
        number = "False"
        while(not(number.isdigit())):
            number = input("Donnez nous le numéro à enlever: ")        
        removePair(name, number)
    elif (answer == 3):
        name = input("Donnez nous le nom à chercher: ")
        findPair(name)
    else:
        repudiation()
            
    cryptData()
if __name__ == "__main__":
    # execute only if run as a script
    main()
